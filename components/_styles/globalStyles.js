import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
    }

    /* Most important part of typography. All typographies will scale to this value, as we use REM.
    First arg of clamp is mobile base font size
    TODO: Second arg should be defined the way it returns exactly min on mobile, max on desktop
    Third arg of clamp is desktop base font size */
    html {
        height: 100%;
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: clamp(${fontSize.s}rem, ${fontSize.s}rem + 0.5vw, ${fontSize.m}rem);
            line-height: clamp(${lineHeight.l}rem, ${lineHeight.l}rem + 0.5vw, ${lineHeight.xl}rem);
        `};
    }

    body {
        ${({ theme: { colors, typographies } }) => `
        color: ${colors.textPrimary};
        font-family: ${typographies.fancy}, ${typographies.default};

        /* Override default button highlight */
        -webkit-tap-highlight-color: ${colors.secondary}20;
        -webkit-touch-callout: ${colors.secondary}20;
        -webkit-user-select: ${colors.secondary}20;
        -khtml-user-select: ${colors.secondary}20;
        -moz-user-select: ${colors.secondary}20;
        -ms-user-select: ${colors.secondary}20;
        user-select: ${colors.secondary}20;
        `};

    }

    h1 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.xxxl}rem;
            line-height: ${lineHeight.xxxl}rem;
        `};
    }

    h2 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.xxl}rem;
            line-height: ${lineHeight.xxl}rem;
        `};
    }

    h3 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.xl}rem;
            line-height: ${lineHeight.xl}rem;
        `};
    }

    h4 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.l}rem;
            line-height: ${lineHeight.l}rem;
        `};
    }

    h5 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.m}rem;
            line-height: ${lineHeight.m}rem;
        `};
    }

    h6 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.s}rem;
            line-height: ${lineHeight.s}rem;
        `};
    }

    img {
        max-width: 100%;
    }

    /* Once you define globalStyles, remember to pass them to this global classes */
    /* They are necessary to handle RichText config (htmlSerializer) */
    &.text-paragraph {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.xs}rem;
            line-height: ${lineHeight.xs}rem;
        `};
    }
    &.text-heading1 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.xxxl}rem;
            line-height: ${lineHeight.xxxl}rem;
        `};
    }

    &.text-heading2 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.xxl}rem;
            line-height: ${lineHeight.xxl}rem;
        `};
    }

    &.text-heading3 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.xl}rem;
            line-height: ${lineHeight.xl}rem;
        `};
    }

    &.text-heading4 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.l}rem;
            line-height: ${lineHeight.l}rem;
        `};
    }

    &.text-heading5 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.m}rem;
            line-height: ${lineHeight.m}rem;
        `};
    }

    &.text-heading6 {
        ${({ theme: { fontSize, lineHeight } }) => `
            font-size: ${fontSize.s}rem;
            line-height: ${lineHeight.s}rem;
        `};
    }
`;
