import styled, { css } from 'styled-components';

const commonText = css`
    color: ${({ theme: { colors }, color }) =>
        color ? colors[color] ?? color : colors.grayDark};
    text-align: ${({ align }) => align ?? 'start'};
    ${({ theme, margin }) =>
        margin &&
        css`
            margin-top: ${typeof margin.top === 'string'
                ? theme.spacing[margin.top]
                : margin.top}rem;
            margin-bottom: ${typeof margin.bottom === 'string'
                ? theme.spacing[margin.bottom]
                : margin.bottom}rem;
        `}
    ${({ theme, size }) =>
        size &&
        css`
            font-size: ${theme.fontSize[size]}rem;
        `}
    ${({ theme, lineHeight }) =>
        lineHeight &&
        css`
            line-height: ${theme.lineHeight[lineHeight]}rem;
        `}
        ${({ theme, weight }) =>
            weight &&
            css`
                font-weight: ${theme.fontWeight[weight]};
            `}
`;

export const Text = styled.p`
    ${commonText}
`;

export const ParagraphText = styled.p`
    ${commonText}
`;

export const TitleText = styled.h3`
    ${commonText}
`;

export const ProminentText = styled.p`
    ${commonText}
    font-family: ${({ theme }) => theme.typographies.prominent};
    text-transform: uppercase;
    font-weight: ${({ theme }) => theme.fontWeight.normal};
`;
