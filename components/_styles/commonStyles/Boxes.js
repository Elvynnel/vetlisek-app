import styled, { css } from 'styled-components';

const commonBox = css`
    height: ${({ h100, height }) =>
        h100 ? '100%' : height ? `${height}px` : 'auto'};
    min-height: ${({ minHeight }) => minHeight && `${minHeight}px`};
    max-height: ${({ maxHeight }) => maxHeight && `${maxHeight}px`};
    width: ${({ w100, width }) =>
        w100 ? '100%' : width ? `${width}px` : 'auto'};
    min-width: ${({ minWidth }) => minWidth && `${minWidth}px`};
    max-width: ${({ maxWidth }) => maxWidth && `${maxWidth}px`};
    ${({ flex }) =>
        flex &&
        css`
            display: ${flex};
        `};
    ${({ flexDirection }) =>
        flexDirection &&
        css`
            flex-direction: ${flexDirection};
        `};
    ${({ alignItems }) =>
        alignItems &&
        css`
            align-items: ${alignItems};
        `};
    ${({ justifyContent }) =>
        justifyContent &&
        css`
            justify-content: ${justifyContent};
        `};
    ${({ theme: { spacing }, margin }) =>
        margin &&
        css`
            margin-top: ${typeof margin.top === 'string'
                ? spacing[margin.top]
                : margin.top}rem;
            margin-bottom: ${typeof margin.bottom === 'string'
                ? spacing[margin.bottom]
                : margin.bottom}rem;
            margin-left: ${typeof margin.left === 'string'
                ? spacing[margin.left]
                : margin.left}rem;
            margin-right: ${typeof margin.right === 'string'
                ? spacing[margin.right]
                : margin.right}rem;
        `}
    ${({ textAlign }) =>
        textAlign &&
        css`
            text-align: ${textAlign};
        `};
    background-color: ${({ theme, color }) =>
        color ? theme.colors[color] ?? color : theme.colors.backgroundPrimary};
`;

export const Box = styled.div`
    ${commonBox};
`;

export const ColumnBox = styled(Box)`
    max-width: ${({ maxWidth }) => maxWidth ?? 720}px;
`;

// TODO: Make this center content
export const MaxWidthContentWrapper = styled(Box)`
    display: block;
    width: 100%;
`;
