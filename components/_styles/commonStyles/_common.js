import theme from 'shared/theme';

// FIXME: Consider more proper names
export const applyStyle = {
    from: {
        cell: `@media (min-width: ${theme.deviceSize.xs}px)`,
        mobile: `@media (min-width: ${theme.deviceSize.sm}px)`,
        tablet: `@media (min-width: ${theme.deviceSize.md}px)`,
        laptop: `@media (min-width: ${theme.deviceSize.lg}px)`,
        monitor: `@media (min-width: ${theme.deviceSize.xl}px)`,
        tv: `@media (min-width: ${theme.deviceSize.xxl}px)`,
    },
    upto: {
        cell: `@media (max-width: ${theme.deviceSize.xs}px)`,
        mobile: `@media (max-width: ${theme.deviceSize.sm}px)`,
        tablet: `@media (max-width: ${theme.deviceSize.md}px)`,
        laptop: `@media (max-width: ${theme.deviceSize.lg}px)`,
        monitor: `@media (max-width: ${theme.deviceSize.xl}px)`,
        tv: `@media (min-width: ${theme.deviceSize.xxl}px)`,
    },
};
