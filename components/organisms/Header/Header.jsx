import { FancyMenu, Logo } from 'components/molecules';
import { SocialMenu } from 'components/molecules/SocialMenu/SocialMenu';
import { applyStyle } from 'components/_styles/commonStyles/_common';
import styled, { css } from 'styled-components';
import { HeaderBackground } from './HeaderBackground';

// FIXME: Consider to create seperate Headers for mobile / desktop
export const Header = () => {
    return (
        <Wrapper>
            <HeaderBackground />
            <Content>
                <LogoWrapper>
                    <Logo />
                </LogoWrapper>
                <SubpagesMenuWrapper>
                    <FancyMenu />
                </SubpagesMenuWrapper>
                <SocialMenuWrapper>
                    <SocialMenu />
                </SocialMenuWrapper>
            </Content>
        </Wrapper>
    );
};

const Wrapper = styled.header`
    ${({ theme: { deviceSize, colors, zIndex, spacing } }) => `
        position: relative;
        display: flex;
        align-items: center;
        width: 100%;
        height: 80px;
        z-index: ${zIndex.header};
        padding-bottom: ${spacing.xs}rem;
        max-width: ${deviceSize.xxl}px;
        border-bottom: 3px ${colors.secondary} solid;
        
        ${applyStyle.from.tablet} {
            height: 200px;
            flex-direction: column;
            border-bottom: none;
        }
    `};
`;

const Content = styled.div(
    ({ theme: { colors } }) => css`
        align-items: center;
        position: relative;
        border-bottom: none;

        ${applyStyle.from.tablet} {
            display: grid;
            border-bottom: 3px ${colors.secondary} solid;
            grid-template-columns: repeat(3, 1fr);
            grid-template-rows: 2fr 1fr;
            grid-template-areas:
                '. logo .'
                '. subpagesMenu socialMenu';
            width: 100%;
        }
    `,
);

const LogoWrapper = styled.div`
    ${applyStyle.from.tablet} {
        grid-area: logo;
        display: flex;
        justify-content: center;
        align-items: center;
    }
`;

const SubpagesMenuWrapper = styled.div`
    display: none;
    ${applyStyle.from.tablet} {
        grid-area: subpagesMenu;
        display: flex;
        justify-content: center;
        align-items: center;
    }
`;

const SocialMenuWrapper = styled.div`
    display: none;
    ${applyStyle.from.tablet} {
        grid-area: socialMenu;
        display: flex;
        justify-content: flex-end;
    }
`;
