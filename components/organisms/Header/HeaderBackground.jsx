import { Icon } from 'components/atomics/Icon/Icon';
import { applyStyle } from 'components/_styles/commonStyles/_common';
import styled from 'styled-components';
import { v4 as uuid } from 'uuid';

const pawConfig = [
    // Left side Paws
    { bottom: 25, left: 2, rotate: 110 },
    { bottom: 40, left: 4, rotate: 100 },

    { bottom: 10, left: 10, rotate: 90 },
    { bottom: 20, left: 12, rotate: 70 },

    { bottom: 20, left: 20, rotate: 92 },
    { bottom: 35, left: 22, rotate: 80 },

    { bottom: 45, left: 30, rotate: 90 },
    { bottom: 55, left: 32, rotate: 75 },

    // Right side Paws
    { bottom: 55, left: 67, rotate: 85 },
    { bottom: 65, left: 69, rotate: 80 },

    { bottom: 65, left: 77, rotate: 90 },
    { bottom: 80, left: 79, rotate: 75 },

    { bottom: 77, left: 87, rotate: 90 },
    { bottom: 90, left: 89, rotate: 90 },

    { bottom: 65, left: 97, rotate: 100 },
    { bottom: 75, left: 99, rotate: 100 },
];

export const HeaderBackground = () => {
    return (
        <Wrapper>
            {pawConfig.map((props, index) => (
                <RotatedPaw key={uuid()} {...props} {...{ index }} />
            ))}
        </Wrapper>
    );
};

// TODO: Correct start time of animation so it won't restart on rerender / navigate (or remove animation)
// We can also make opacity 'gradient', eg. /* opacity: clamp(0.4, ${index * 0.1}, 0.7); */
const RotatedPaw = styled(Icon).attrs(({ rotate }) => ({
    size: 12,
    name: 'Paw',
    rotate: rotate ?? 80,
}))`
    ${({ bottom, left, index }) => `
        position: absolute;
        bottom: ${bottom}%;
        left: ${left}%;
        opacity: 0;

        animation: 1s ease ${200 * index}ms beast-walks;
        animation-fill-mode: forwards;
        
        @keyframes beast-walks {
            0% {
                opacity: 0.2;
            }
            100% {
                opacity: 0.7;
            }
        }
    `};
`;

const Wrapper = styled.div`
    ${({ theme: { zIndex } }) => `
        position: absolute;
        width: 100%;
        height: 100%;
        z-index: ${zIndex.negative};

        ${applyStyle.upto.tablet} {
            display: none;
        }
    `};
`;
