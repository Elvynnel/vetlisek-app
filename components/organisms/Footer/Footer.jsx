import { BasicList, Logo as logo } from 'components/molecules';
import { SocialMenu } from 'components/molecules/SocialMenu/SocialMenu';
import { MaxWidthContentWrapper } from 'components/_styles/commonStyles/Boxes';
import { Text } from 'components/_styles/commonStyles/Texts';
import { applyStyle } from 'components/_styles/commonStyles/_common';
import styled, { css } from 'styled-components';

export const Footer = () => {
    const mainMenuList = [
        {
            text: 'Home',
            path: '/home',
        },
        {
            text: 'Blog',
            path: '/blog',
        },
        {
            text: 'O mnie',
            path: '/about',
        },
        {
            text: 'Szkolenia',
            path: '/trainings',
        },
    ];

    const contactList = [
        {
            text: 'Tel. +48 678 678 678',
        },
        {
            text: 'E-mail: kontakt@vetlisek.pl',
        },
    ];

    return (
        <Wrapper>
            <MaxWidthContentWrapper
                color="tertiary"
                margin={{ top: 'l', bottom: 'l' }}
            >
                <Main>
                    <Logo />
                    <MainMenu title="menu" list={mainMenuList} />
                    <Contact title="kontakt" list={contactList} />
                    <SocialMenuWrapper>
                        <MediaText>Follow me</MediaText>
                        <SocialMenu />
                    </SocialMenuWrapper>
                </Main>
            </MaxWidthContentWrapper>

            <MaxWidthContentWrapper color="primary">
                <Copyright>
                    <Text color="textQuarternary">
                        @2019 VETLISEK. all right reserved.{' '}
                    </Text>
                    <Text color="textQuarternary">
                        Designed by: Katarzyna Szwarc-Pawłowska
                    </Text>
                </Copyright>
            </MaxWidthContentWrapper>
        </Wrapper>
    );
};

const Wrapper = styled.footer(
    ({ theme: { colors } }) => css`
        width: 100%;
        display: flex;
        flex-direction: column;
        background-color: ${colors.tertiary};
    `,
);

const Main = styled.div(
    ({ theme: { colors, spacing } }) => css`
        display: grid;
        grid-gap: ${spacing.s}rem;
        grid-template-areas:
            'logo social'
            'contact .';
        padding: ${spacing.xs}rem;
        background-color: ${colors.tertiary};

        ${applyStyle.from.tablet} {
            grid-template-columns: repeat(4, 1fr);
            grid-template-rows: repeat(2, 1fr);
            grid-template-areas:
                'logo menu contact social'
                '. menu . .';
            padding: ${spacing.xs}rem ${spacing.xxl}rem;
        }
    `,
);

const Logo = styled(logo)`
    grid-area: logo;
`;

const MainMenu = styled(BasicList)`
    grid-area: menu;
    display: none;

    ${applyStyle.from.tablet} {
        display: grid;
    }
`;

const Contact = styled(BasicList)`
    grid-area: contact;
`;

const SocialMenuWrapper = styled.section`
    grid-area: social;
`;

const MediaText = styled(Text)(
    ({ theme: { colors, spacing } }) => css`
        display: none;
        text-transform: uppercase;
        margin-bottom: ${spacing.xxs}rem;
        color: ${colors.textTertiary};

        ${applyStyle.from.tablet} {
            display: block;
        }
    `,
);

const Copyright = styled.section(
    ({ theme: { colors, spacing, deviceSize } }) => css`
        grid-area: bot;

        display: flex;
        justify-content: space-between;
        align-items: content;
        padding: ${spacing.s}rem ${spacing.xxl}rem;
        max-width: ${deviceSize.xxl};
        color: ${colors.textQuarternary};
        background-color: ${colors.primary};
    `,
);
