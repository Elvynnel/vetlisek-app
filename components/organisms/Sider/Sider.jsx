import { Image, TitleBar } from 'components/atomics';
import { Box } from 'components/_styles/commonStyles/Boxes';
import { Text } from 'components/_styles/commonStyles/Texts';
// import { lidia } from 'public/images';
import { RichText } from 'prismic-reactjs';
import * as Styled from './styles';

export const Sider = ({ aboutMe }) => {
    const { photo, name, description } = aboutMe ?? {};
    return (
        <Styled.Aside>
            <Styled.AboutWrapper>
                <TitleBar>o mnie</TitleBar>
                <Image
                    src={photo?.url}
                    alt={name}
                    margin={{ top: 's', bottom: 'm' }}
                />
                <Text color="primary">{name}</Text>
                <Box textAlign="center" margin={{ top: 's', bottom: 's' }}>
                    {description && <RichText render={description} />}
                </Box>
            </Styled.AboutWrapper>

            <Styled.InstagramTitleBar margin={{ bottom: 'm' }}>
                instagram
            </Styled.InstagramTitleBar>
            <Styled.InstagramPhotosContainer>
                {[
                    { id: 1 },
                    { id: 2 },
                    { id: 3 },
                    { id: 4 },
                    { id: 5 },
                    { id: 6 },
                ].map(({ id }) => (
                    <Image
                        key={id}
                        src={`https://picsum.photos/id/${id}/191/191`}
                        alt={`instagram${id}`}
                    ></Image>
                ))}
            </Styled.InstagramPhotosContainer>
        </Styled.Aside>
    );
};
