import { TitleBar } from 'components/atomics';
import { applyStyle } from 'components/_styles/commonStyles/_common';
import styled from 'styled-components';

export const Aside = styled.aside`
    display: flex;
    height: 100%;
    flex-direction: row;

    ${applyStyle.from.tablet} {
        flex-direction: column;
    }
`;

export const MostRead = styled.section`
    display: none;

    ${applyStyle.from.tablet} {
        display: block;
        padding-bottom: ${({ theme }) => theme.spacing.l}rem;
    }
`;

export const AboutWrapper = styled.section`
    display: none;

    ${applyStyle.from.tablet} {
        display: flex;
        flex-direction: column;
        align-items: center;
    }
`;

export const AsideItem = styled.article`
    flex: 1;
    background-color: red;
`;

export const InstagramTitleBar = styled(TitleBar)`
    display: none;

    ${applyStyle.from.tablet} {
        display: flex;
    }
`;

export const InstagramPhotosContainer = styled.section`
    display: grid;
    grid-template-columns: repeat(6, 1fr);
    grid-gap: ${({ theme: { spacing } }) => `${spacing.s}rem ${spacing.m}rem`};

    ${applyStyle.from.tablet} {
        display: grid;
        grid-template-columns: 1fr 1fr;
        grid-gap: ${({ theme: { spacing } }) =>
            `${spacing.s}rem ${spacing.m}rem`};
    }
`;
