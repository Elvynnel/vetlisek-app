import { Icon } from 'components/atomics/Icon/Icon';
import styled from 'styled-components';

export const SocialMenu = ({ className }) => {
    return (
        <Wrapper {...{ className }}>
            <Icon size={22} name="Facebook" />
            <Icon size={22} name="Instagram" />
            <Icon size={22} name="LinkedIn" />
        </Wrapper>
    );
};

const Wrapper = styled.section`
    display: grid;
    grid-template-columns: repeat(3, 22px);
    grid-gap: ${({ theme: { spacing } }) => spacing.s}rem;
    align-items: center;
`;
