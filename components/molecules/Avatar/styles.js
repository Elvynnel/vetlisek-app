import { Image } from 'components/atomics/Image/styled';
import styled from 'styled-components';

export const Avatar = styled(Image)`
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    height: ${({ size }) => size}px;
    width: ${({ size }) => size}px;
    border-radius: 50%;
`;
