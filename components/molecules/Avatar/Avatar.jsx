import * as Styled from './styles';

export const Avatar = (props) => {
    return <Styled.Avatar {...props} />;
};
