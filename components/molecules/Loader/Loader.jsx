import { Icon } from 'components/atomics';
import React from 'react';
import styled from 'styled-components';

export const Loader = () => {
    return (
        <Wrapper>
            <LoaderIcon name="Paw" size={30} />
        </Wrapper>
    );
};

const Wrapper = styled.div`
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const LoaderIcon = styled(Icon)`
    opacity: 0.2;
    animation: 3s ease 0.1s blink;
    animation-iteration-count: infinite;

    @keyframes blink {
        0%,
        100% {
            opacity: 0.2;
        }
        40%,
        60% {
            opacity: 1;
        }
    }
`;
