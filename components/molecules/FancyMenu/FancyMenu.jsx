import { ProminentText } from 'components/_styles/commonStyles/Texts';
import Link from 'next/link';
import styled from 'styled-components';
import { v4 as uuid } from 'uuid';

const menuItems = [
    {
        text: 'Blog',
        path: '/',
    },
    // {
    //     text: 'Warsztaty',
    //     path: '/workshops',
    // },
    {
        text: 'O mnie',
        path: '/about',
    },
];

export const FancyMenu = ({ menu = menuItems, variant }) => {
    return (
        <Wrapper {...{ variant }}>
            {menu.map(({ text, path }) => (
                <Link key={uuid()} href={path}>
                    <MenuItem {...{ variant }}>{text}</MenuItem>
                </Link>
            ))}
        </Wrapper>
    );
};

const Wrapper = styled.nav`
    ${({ theme: { spacing }, variant }) => `
        display: grid;
        grid-auto-columns: 1fr;
        grid-auto-flow: ${variant === 'secondary' ? 'row' : 'column'};
        grid-gap: ${spacing[variant === 'secondary' ? 'm' : 'xl']}rem;
    `};
`;

const MenuItem = styled(ProminentText)`
    ${({ theme: { fontSize, colors, transition, spacing }, variant }) => `
        padding: ${
            variant === 'secondary' ? `0 ${spacing.l}rem` : `${spacing.s}rem 0`
        };
        font-size: ${fontSize.m}rem;
        text-decoration: none;
        color: ${colors.primary};
        text-transform: uppercase;
        cursor: pointer;
        align-self: center;
        justify-self: center;
        border-color: ${colors.primary};
        transition: all ${transition.quick}s ease-in-out; 

        &:hover {
            color: ${colors.secondary};
        }

        &:after {
            display:block;
            content: '';
            border-bottom: solid 2px ${colors.primary};  
            transform: scaleX(0);  
            transition: all ${transition.default}s ease-in-out;
        }
        
        &:hover:after { 
            border-color: ${colors.secondary};
            transform: scaleX(1); 
        }
    `};
`;
