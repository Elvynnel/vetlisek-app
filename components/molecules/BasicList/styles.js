import { Text } from 'components/_styles/commonStyles/Texts';
import styled from 'styled-components';

export const BasicList = styled.section`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

export const ListTitle = styled(Text)`
    text-transform: uppercase;
    color: ${({ theme }) => theme.colors.textTertiary};
`;

export const ListItem = styled(Text)`
    margin-bottom: ${({ theme }) => theme.spacing.xs}rem;
    color: ${({ theme }) => theme.colors.textTertiary};
`;
