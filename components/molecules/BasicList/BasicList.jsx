import { Text } from 'components/_styles/commonStyles/Texts';
import { applyStyle } from 'components/_styles/commonStyles/_common';
import styled, { css } from 'styled-components';

export const BasicList = ({ title, className, list = [] }) => {
    return (
        <Wrapper {...{ className }}>
            {title && <ListTitle>{title}</ListTitle>}

            {list.map(({ text, path }, index) => (
                <ListItem key={index} as={path && 'a'} href={path}>
                    {text}
                </ListItem>
            ))}
        </Wrapper>
    );
};

const Wrapper = styled.section`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

const ListTitle = styled(Text)(
    ({ theme: { colors } }) => css`
        text-transform: uppercase;
        color: ${colors.textTertiary};
    `,
);

const ListItem = styled(Text)(
    ({ theme: { colors, spacing } }) => css`
        color: ${colors.textTertiary};

        ${applyStyle.from.tablet} {
            margin-bottom: ${spacing.xs}rem;
        }
    `,
);
