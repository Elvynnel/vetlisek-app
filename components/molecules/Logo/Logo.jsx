import { Text } from 'components/_styles/commonStyles/Texts';
import { applyStyle } from 'components/_styles/commonStyles/_common';
import Link from 'next/link';
import styled from 'styled-components';

export const Logo = ({ className, signatured }) => {
    return (
        <Link href="/">
            <Wrapper {...{ className }}>
                {/* TODO: Move to icons or use next/image */}
                <img src="/images/vetlisekLogo.svg" alt="logo" />

                {signatured && <Signature>Lek. wet. Lidia Nosal</Signature>}
            </Wrapper>
        </Link>
    );
};

const Wrapper = styled.section`
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    justify-content: center;
    width: 200px;
    cursor: pointer;

    ${applyStyle.from.tablet} {
        width: 320px;
    }
`;

const Signature = styled(Text)`
    ${({ theme: { colors, fontWeight } }) => `
        font-size: 17px;
        color: ${colors.primary};
        font-weight: ${fontWeight.thin};
    `}
`;
