import { Icon } from 'components/atomics';
import React from 'react';
import styled from 'styled-components';

export const IconButton = ({ onClick, ...props }) => {
    return (
        <Wrapper {...{ onClick }}>
            <Icon {...props} />
        </Wrapper>
    );
};

const Wrapper = styled.button`
    background-color: transparent;
    border-color: transparent;
    cursor: pointer;

    &:focus {
        outline: none;
    }

    &:active {
        opacity: 0.8;
    }
`;
