import { Box } from 'components/_styles/commonStyles/Boxes';
import { Text, TitleText } from 'components/_styles/commonStyles/Texts';
import Link from 'next/link';
import PropTypes from 'prop-types';
import React from 'react';
import styled, { css } from 'styled-components';

export const ArticlePreview = ({
    title = 'Article title nice vet med topic',
    tags = ['tag', 'tag'],
    creationDate = '11 listopada 2019',
    author = 'Lek. wet. Lidia Nosal',
    description = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat',
    pictureConfig = { url: 'https://picsum.photos/260/190', alt: 'picture' },
    variant = 'default',
    route = '/',
}) => {
    return (
        <Wrapper {...{ variant }}>
            <Cover url={pictureConfig.url} {...{ variant }} />
            <TextsWrapper {...{ variant }}>
                <TagsWrapper>
                    {tags.map((tag, index) => (
                        <Text key={index} color="secondary">
                            {`#${tag} `}
                        </Text>
                    ))}
                </TagsWrapper>
                <Link href={route}>
                    <TitleText
                        as="a"
                        // FIXME: To be styled + find better solution for clickable text
                        href={route}
                    >
                        {title}
                    </TitleText>
                </Link>

                <Box flex="flex" flexDirection="row" margin={{ bottom: 's' }}>
                    {variant !== 'exposed' && (
                        <Text size="xs" color="primary">
                            {`${author}`}
                        </Text>
                    )}
                    {variant === 'default' && (
                        <Text size="xs" color="primary">
                            {' '}
                            &middot;{' '}
                        </Text>
                    )}
                    <Text size="xs" color="primary">
                        {`${creationDate}`}
                    </Text>
                </Box>
                <Text color="textSecondary" {...{ variant }}>
                    {description}
                </Text>
            </TextsWrapper>
        </Wrapper>
    );
};

ArticlePreview.propTypes = {
    variant: PropTypes.oneOf(['default', 'exposed']),
};

const Wrapper = styled.div(
    ({ theme: { spacing }, variant }) => css`
        overflow: hidden;
        text-overflow: elipsis;

        ${variant === 'exposed' &&
        css`
            display: grid;
            grid-template-columns: 2fr 3fr;
        `}

        ${variant === 'default' &&
        css`
            height: ${3 * spacing.xxl}rem;
            display: grid;
            grid-template-columns: 1fr 2fr;
        `}
    `,
);

const Cover = styled.div(
    ({ theme: { spacing }, url }) => css`
        min-width: ${2 * spacing.xxl}rem;
        background-image: url(${url});
        background-repeat: no-repeat;
        background-size: cover;
    `,
);

const TagsWrapper = styled.section`
    display: flex;
`;

const TextsWrapper = styled.section(
    ({ theme: { spacing } }) => css`
        white-space: pre-wrap;
        margin: ${`0 0 ${spacing.xl}rem ${spacing.s}rem`};
    `,
);
