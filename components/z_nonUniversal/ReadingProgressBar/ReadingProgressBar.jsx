import React from 'react';
import { useReadingProgress } from 'shared/hooks/useReadingProgress';
import styled from 'styled-components';

export const ReadingProgressBar = () => {
    const progress = useReadingProgress();

    // IMPORTANT: Progress variable needs to be in inline style due too much css classes generation by styled components
    return <Wrapper style={{ width: `${progress}%` }} />;
};

const Wrapper = styled.div`
    ${({ theme: { colors } }) => `
        height: 3px;
        position: fixed;
        top: 0;
        left: 0;
        background-color: ${colors.progress};
    `}
`;
