import { Icon } from 'components/atomics';
import { Text } from 'components/_styles/commonStyles/Texts';
import { applyStyle } from 'components/_styles/commonStyles/_common';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

export const MenuButton = ({ isMenuExpanded, onClick }) => {
    const [isMenuButtonDisabled, setIsMenuButtonDisabled] = useState(false);

    // TODO: Find better solution for blocking menu button on overlay click
    // - when clicking on link, it also redirects so this is not proper solution. Bubbling?
    useEffect(() => {
        setIsMenuButtonDisabled(true);
        setTimeout(() => setIsMenuButtonDisabled(false), 300);
    }, [isMenuExpanded === false]);

    return (
        <MenuIconWrapper {...{ onClick }} disabled={isMenuButtonDisabled}>
            <MenuIcon size={22} name="Paw" {...{ isMenuExpanded }} />
            <Text color="secondary" size="xxxs" margin={{ top: 'xs' }}>
                MENU
            </Text>
        </MenuIconWrapper>
    );
};

const MenuIconWrapper = styled.button`
    ${({ theme: { zIndex } }) => `
        position: absolute;
        right: 20px;
        top: 20px;
        display: flex;
        flex-direction: column;
        align-items: center;
        z-index: ${zIndex.menuButton};
        background-color: transparent;
        border-color: transparent;
        cursor: pointer;
        
        &:focus {
            outline: none;
        }

        ${applyStyle.from.tablet} {
            display: none;
        }
    `}
`;

const MenuIcon = styled(Icon)`
    ${({ isMenuExpanded, theme: { transition } }) => `
        transform: ${isMenuExpanded && 'rotate(90deg)'};
        transition: transform ${transition.default}s ease-out; 
    `}
`;
