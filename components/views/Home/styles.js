import { Sider as Aside } from 'components/organisms';
import { Box } from 'components/_styles/commonStyles/Boxes';
import { applyStyle } from 'components/_styles/commonStyles/_common';
import styled, { css } from 'styled-components';

export const HomePage = styled.main`
    max-width: ${({ theme: { deviceSize } }) => deviceSize.xxl}px;
`;

export const MainContent = styled.main`
    width: 100%;
    display: grid;
    gap: 10px;
    padding-top: ${({ theme }) => theme.spacing.xl}rem;
    grid-template-rows: 1fr auto;

    ${applyStyle.from.tablet} {
        grid-template-columns: 1fr 398px;
    }
`;

export const ExposedArticleWrapper = styled(Box)(
    ({ theme: { spacing, colors } }) => css`
        padding: ${spacing.xl}rem 0px;
        border-bottom: 1px solid ${colors.borderLight};
        ${applyStyle.upto.tablet} {
            display: none;
        }
    `,
);

export const MainArticlesWrapper = styled.section(
    ({ theme: { spacing } }) => css`
        display: grid;
        grid-gap: ${spacing.s}rem;
    `,
);

export const Sider = styled(Aside)`
    grid-column-end: -1;
`;
