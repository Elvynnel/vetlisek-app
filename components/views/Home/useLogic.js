// import axios from 'axios';
import Prismic from 'prismic-javascript';
import { useEffect, useState } from 'react';
import { Client } from '../../../prismic-configuration';

export const useLogic = () => {
    const [articlesWithPagination, setArticlesWithPagination] = useState(null);
    const [aboutMe, setAboutMe] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const articles = await Client().query(
                [Prismic.Predicates.at('document.type', 'blog')],
                {
                    pageSize: 25,
                    page: 1,
                    orderings: '[my.blog-post.date desc]',
                },
            );
            console.log({ articles });
            setArticlesWithPagination(articles);

            const aboutMe = await Client().getSingle('about_me');
            console.log({ aboutMe });
            setAboutMe(aboutMe.data);
        };

        fetchData();
    }, []);

    const articles = articlesWithPagination?.results ?? [];

    // To be checked: Make sure that calculation work well with pagination
    const exposedArticle = articles?.sort(
        (prev, next) =>
            new Date(next.last_publication_date) -
            new Date(prev.last_publication_date),
    )[0];

    return { articles, exposedArticle, aboutMe };
};
