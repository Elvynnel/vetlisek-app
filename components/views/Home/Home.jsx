import { ArticlePreview } from 'components/molecules';
import { Layout } from 'components/templates';
import { isArrayEmpty } from 'shared/functions/isArrayEmpty';
import * as Styled from './styles';
import { useLogic } from './useLogic';
function Home() {
    const { articles, exposedArticle, aboutMe } = useLogic();
    return (
        // FIXME: This is in fact not a accurate condition, but it works.
        // We should probably keep loading state in global store or prepare basic status generator (context?)
        <Layout isLoading={isArrayEmpty(articles)}>
            <Styled.HomePage>
                {exposedArticle && (
                    <Styled.ExposedArticleWrapper key={exposedArticle.id}>
                        <ArticlePreview
                            variant="exposed"
                            tags={exposedArticle.tags}
                            title={exposedArticle.data.title[0]?.text}
                            creationDate={exposedArticle.data.date}
                            description={exposedArticle.data.content[0]?.text}
                            pictureConfig={exposedArticle.data.cover}
                            route={`/articles/${exposedArticle.uid}`}
                        />
                    </Styled.ExposedArticleWrapper>
                )}
                <Styled.MainContent>
                    <Styled.MainArticlesWrapper>
                        {articles.map(
                            ({
                                id,
                                tags,
                                uid,
                                data: { title, date, content, cover },
                            }) => (
                                <ArticlePreview
                                    key={id}
                                    tags={tags}
                                    title={title[0]?.text}
                                    creationDate={date}
                                    description={content[0]?.text}
                                    pictureConfig={cover}
                                    route={`/articles/${uid}`}
                                />
                            ),
                        )}
                    </Styled.MainArticlesWrapper>

                    <Styled.Sider {...{ aboutMe }} />
                </Styled.MainContent>
            </Styled.HomePage>
        </Layout>
    );
}

Home.getInitialProps = async ({ req }) => {
    // TODO:
};

export default Home;
