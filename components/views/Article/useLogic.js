import axios from 'axios';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

export const useLogic = () => {
    const [article, setArticle] = useState(null);
    const router = useRouter();
    const { uid } = router.query;

    useEffect(() => {
        const fetchData = async () => {
            const main = await axios('https://vetlisek.cdn.prismic.io/api/v2');
            const ref = main.data.refs[0].ref;

            const { data } = await axios(
                `https://vetlisek.cdn.prismic.io/api/v2/documents/search?ref=${ref}`,
            );
            setArticle(data?.results.find((article) => article.uid === uid));
        };

        fetchData();
    }, [uid]);

    return { article };
};
