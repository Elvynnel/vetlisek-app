import { Box } from 'components/_styles/commonStyles/Boxes';
import { Text } from 'components/_styles/commonStyles/Texts';
import styled from 'styled-components';

export const MediaText = styled(Text)`
    margin-right: ${({ theme: { spacing } }) => spacing.s}rem;
    color: ${({ theme }) => theme.colors.textSecondary};
    font-weight: ${({ theme }) => theme.fontWeight.thin};
`;

export const ContentWrapper = styled(Box)`
    margin: ${({ theme: { spacing } }) => `${spacing.l}rem 0`};
`;
