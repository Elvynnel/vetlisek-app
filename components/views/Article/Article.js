import { Image } from 'components/atomics';
import { Avatar } from 'components/molecules/Avatar/Avatar';
import { SocialMenu } from 'components/molecules/SocialMenu/SocialMenu';
import { Layout } from 'components/templates/Layout/Layout';
import { Box, ColumnBox } from 'components/_styles/commonStyles/Boxes';
import {
    ParagraphText,
    Text,
    TitleText,
} from 'components/_styles/commonStyles/Texts';
import { RichText } from 'prismic-reactjs';
import * as Styled from './styles';
import { useLogic } from './useLogic';

export default function Post() {
    const { article } = useLogic();
    return (
        <Layout isLoading={!article} isReadingProgress>
            <ColumnBox margin={{ top: 'xl', left: 'm', right: 'm' }}>
                <TitleText margin={{ bottom: 's' }}>
                    {article?.data.title[0].text}
                </TitleText>
                <ParagraphText margin={{ bottom: 'm' }}>
                    {article?.data.content[0].text}
                </ParagraphText>
                <Box
                    flex="flex"
                    justifyContent="space-between"
                    margin={{ bottom: 's' }}
                >
                    <Box flex="flex">
                        <Avatar src="https://picsum.photos/50/50" size={45} />
                        <Box
                            flex="flex"
                            flexDirection="column"
                            justifyContent="space-between"
                            margin={{ left: 's' }}
                        >
                            <Text color="secondary" size="xs" lineHeight="xs">
                                lek. wet. Lidia Nosal
                            </Text>
                            <Text color="primary" size="xs" lineHeight="xs">
                                {article?.data.date}
                            </Text>
                        </Box>
                    </Box>
                    <Box flex="flex" alignItems="center">
                        <Styled.MediaText>Share:</Styled.MediaText>
                        <SocialMenu />
                    </Box>
                </Box>
                <Image
                    src={article?.data.cover.url}
                    alt={article?.data.cover.alt}
                />
                <Styled.ContentWrapper>
                    {article?.data.content && (
                        <RichText
                            render={article?.data.content}
                            // htmlSerializer={htmlSerializer}
                        />
                    )}
                </Styled.ContentWrapper>
            </ColumnBox>
        </Layout>
    );
}
