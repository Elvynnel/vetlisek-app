import styled from 'styled-components';

export const ExternalLink = ({ text }) => {
    return <Wrapper>{text}</Wrapper>;
};

const Wrapper = styled.a`
    ${({ theme: { spacing } }) => `
        margin: 0 ${spacing.s}rem;
        text-transform: uppercase;
    `}
`;
