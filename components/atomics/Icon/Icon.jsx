import React from 'react';
import * as Icons from 'shared/icons';
import styled, { css } from 'styled-components';

export const Icon = ({
    name,
    primaryColor,
    secondaryColor,
    size,
    rotate,
    className,
}) => {
    const Icon = Icons[name];
    return (
        <Wrapper {...{ size, rotate, className }}>
            <Icon {...{ primaryColor, secondaryColor }} />
        </Wrapper>
    );
};

const Wrapper = styled.div`
    display: flex;
    ${({ size }) =>
        size &&
        css`
            font-size: ${size}px;
        `}
	transform: rotate(${({ rotate }) => rotate ?? 0}deg);
`;
