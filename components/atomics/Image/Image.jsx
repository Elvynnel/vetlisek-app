import React from 'react';
import * as Styled from './styled';

export const Image = ({
    src,
    alt,
    width,
    minWidth,
    maxWidth,
    height,
    minHeight,
    maxHeight,
    objectFit,
    backgroundColor,
    margin,
}) => (
    <Styled.Wrapper
        {...{
            width,
            minWidth,
            maxWidth,
            height,
            minHeight,
            maxHeight,
            backgroundColor,
            margin,
        }}
    >
        <Styled.Image {...{ src, alt, objectFit }} />
    </Styled.Wrapper>
);
