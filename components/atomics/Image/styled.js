import { Box } from 'components/_styles/commonStyles/Boxes';
import styled from 'styled-components';

export const Wrapper = styled(Box)`
    overflow: hidden;
`;

export const Image = styled.img`
    width: 100%;
    height: 100%;
`;
