import { ProminentText } from 'components/_styles/commonStyles/Texts';
import styled, { css } from 'styled-components';

export const TitleBar = styled(ProminentText)(
    ({ theme: { spacing, fontSize, lineHeight, typographies, colors } }) => css`
        display: flex;
        width: 100%;
        align-items: center;
        justify-content: center;
        padding: ${spacing.s}rem 0;
        font-size: ${fontSize.l}rem;
        line-height: ${lineHeight.l}rem;
        font-family: ${typographies.prominent};
        color: ${colors.textQuarternary};
        background-color: ${colors.primary};
    `,
);
