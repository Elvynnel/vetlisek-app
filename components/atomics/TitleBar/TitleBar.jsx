import * as Styled from './styles';

export const TitleBar = ({ children, ...props }) => {
    return <Styled.TitleBar {...props}>{children}</Styled.TitleBar>;
};
