import { Text } from 'components/_styles/commonStyles/Texts';
import React from 'react';
import styled from 'styled-components';

export const Tag = ({ children }) => {
    return <Wrapper>{children}</Wrapper>;
};

const Wrapper = styled(Text)`
    ${({ theme: { colors, borderRadius, fontSize, fontWeight } }) => `
        display: flex;
        align-self: center;
        justify-content: center;
        align-items: center;
        height: 41px;
        padding: 0px 10px;
        margin: 0px 5px 10px;
        background-color: ${colors.secondary};
        border-radius: ${borderRadius.s}px;

        size: ${fontSize.m}px;
        font-weight: ${fontWeight.thin};
        color: ${colors.textQuarternary};
    `}
`;
