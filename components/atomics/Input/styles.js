import styled from 'styled-components';

export const Input = styled.input`
    height: 35px;
    border: ${({ theme }) => `1px solid ${theme.colors.borderLight}`};

    &:focus {
        border: ${({ theme }) => `1px solid ${theme.colors.primary}`};
        outline: ${({ theme }) => `${theme.colors.primary}50 solid 5px`};
    }
`;
