import { FancyMenu } from 'components/molecules';
import { Loader } from 'components/molecules/Loader/Loader';
import { Footer, Header } from 'components/organisms';
import { MenuButton } from 'components/z_nonUniversal/MenuButton/MenuButton';
import { ReadingProgressBar } from 'components/z_nonUniversal/ReadingProgressBar/ReadingProgressBar';
import { applyStyle } from 'components/_styles/commonStyles/_common';
import React, { useRef, useState } from 'react';
import { useOutsideClick } from 'shared/hooks/useOutsideClick';
import styled from 'styled-components';

export const Layout = ({ children, isLoading, isReadingProgress }) => {
    const [isMenuExpanded, setIsMenuExpanded] = useState(false);
    const menuRef = useRef(null);
    useOutsideClick(menuRef, () => setIsMenuExpanded(false));

    return (
        <>
            {isReadingProgress && <ReadingProgressBar />}
            <Drawer ref={menuRef} isMenuExpanded={isMenuExpanded}>
                <FancyMenu variant="secondary" />
            </Drawer>
            {isMenuExpanded && <Overlay isMenuExpanded={true} />}
            {isLoading ? (
                // TODO: Update Loader (implementation, UI and cache)
                <Loader />
            ) : (
                <Wrapper>
                    <MenuButton
                        onClick={() =>
                            setIsMenuExpanded(
                                (prevIsMenuExpanded) => !prevIsMenuExpanded,
                            )
                        }
                        {...{ isMenuExpanded }}
                    />
                    <Header />
                    <MainContent>{children}</MainContent>
                    <Footer />
                </Wrapper>
            )}
        </>
    );
};

const MainContent = styled.main`
    ${({ theme: { spacing } }) => `
        display: flex;
        margin: ${spacing.xs}rem;

        ${applyStyle.from.tablet} {
            margin: ${spacing.xxl}rem;
        }
    `}
`;

const Drawer = styled.section`
    ${({
        theme: { colors, transition, zIndex },
        isMenuExpanded,
        drawerWidth = 240,
    }) => `
        display: flex;
        align-items: center;
        justify-content: center;
        height: 100%;
        width: ${drawerWidth}px;
        position: fixed;
        left: ${isMenuExpanded ? '0' : `-${drawerWidth}px`};
        visibility: ${isMenuExpanded ? 'visible' : 'hidden'};
        z-index: ${zIndex.sidebar};
        transition: all ${transition.default}s;
        background-color: ${colors.backgroundPrimary};
    `}
`;

const Wrapper = styled.div`
    ${() => `
        display: flex;
        flex-direction: column;
        align-items: center;
    `}
`;

const Overlay = styled.div`
    ${({ theme: { colors, zIndex }, isMenuExpanded }) => `
        display: block;
        width: 100%;
        height: 100%;
        position: fixed;
        z-index: ${zIndex.overlay};
        top: 0;
        visibility: ${isMenuExpanded ? 'visible' : 'hidden'};
        opacity: ${isMenuExpanded ? '0.4' : '0'};
        background-color: ${colors.backgroundSecondary};
    `}
`;
