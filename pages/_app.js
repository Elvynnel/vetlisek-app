import { GlobalStyle } from 'components/_styles/globalStyles';
import App from 'next/app';
import theme from 'shared/theme';
import { ThemeProvider } from 'styled-components';
export default class MyApp extends App {
    render() {
        const { Component, pageProps } = this.props;
        return (
            <ThemeProvider {...{ theme }}>
                <GlobalStyle />
                <Component {...pageProps} />
            </ThemeProvider>
        );
    }
}
