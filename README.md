# Vetlisek

Blog for vets.

- [Vetlisek](#vetlisek)
  - [Example app with styled-components](#example-app-with-styled-components)
  - [Deploy your own](#deploy-your-own)
  - [How to use](#how-to-use)
    - [Using `create-next-app`](#using-create-next-app)
    - [Download manually](#download-manually)
    - [Try it on CodeSandbox](#try-it-on-codesandbox)
    - [Notes](#notes)
    - [Developer flags](#developer-flags)

## Example app with styled-components

This example features how you use a different styling solution than [styled-jsx](https://github.com/zeit/styled-jsx) that also supports universal styles. That means we can serve the required styles for the first render within the HTML and then load the rest in the client. In this case we are using [styled-components](https://github.com/styled-components/styled-components).

For this purpose we are extending the `<Document />` and injecting the server side rendered styles into the `<head>`, and also adding the `babel-plugin-styled-components` (which is required for server side rendering). Additionally we set up a global [theme](https://www.styled-components.com/docs/advanced#theming) for styled-components using NextJS custom [`<App>`](https://nextjs.org/docs/advanced-features/custom-app) component.

## Deploy your own

Deploy the example using [Vercel](https://vercel.com):

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/import/project?template=https://github.com/vercel/next.js/tree/canary/examples/vet-lisek)

## How to use

### Using `create-next-app`

Execute [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) with [npm](https://docs.npmjs.com/cli/init) or [Yarn](https://yarnpkg.com/lang/en/docs/cli/create/) to bootstrap the example:

```bash
npx create-next-app --example vet-lisek vet-lisek-app
# or
yarn create next-app --example vet-lisek vet-lisek-app
```

### Download manually

Download the example:

```bash
curl https://codeload.github.com/vercel/next.js/tar.gz/canary | tar -xz --strip=2 next.js-canary/examples/vet-lisek
cd vet-lisek
```

Install it and run:

```bash
npm install
npm run dev
# or
yarn
yarn dev
```

Deploy it to the cloud with [Vercel](https://vercel.com/import?filter=next.js&utm_source=github&utm_medium=readme&utm_campaign=next-example) ([Documentation](https://nextjs.org/docs/deployment)).

### Try it on CodeSandbox

[Open this example on CodeSandbox](https://codesandbox.io/s/github/vercel/next.js/tree/canary/examples/vet-lisek)

### Notes

When wrapping a [Link](https://nextjs.org/docs/api-reference/next/link) from `next/link` within a styled-component, the [as](https://styled-components.com/docs/api#as-polymorphic-prop) prop provided by `styled` will collide with the Link's `as` prop and cause styled-components to throw an `Invalid tag` error. To avoid this, you can either use the recommended [forwardedAs](https://styled-components.com/docs/api#forwardedas-prop) prop from styled-components or use a different named prop to pass to a `styled` Link.

<details>
<summary>Click to expand workaround example</summary>
<br />

**components/StyledLink.js**

```javascript
import Link from 'next/link';
import styled from 'styled-components';

const StyledLink = ({ as, children, className, href }) => (
    <Link href={href} as={as} passHref>
        <a className={className}>{children}</a>
    </Link>
);

export default styled(StyledLink)`
    color: #0075e0;
    text-decoration: none;
    transition: all 0.2s ease-in-out;

    &:hover {
        color: #40a9ff;
    }

    &:focus {
        color: #40a9ff;
        outline: none;
        border: 0;
    }
`;
```

**pages/index.jsx**

```javascript
import StyledLink from '../components/StyledLink';

export default () => (
    <StyledLink href="/post/[pid]" forwardedAs="/post/abc">
        First post
    </StyledLink>
);
```

</details>

### Developer flags

**TODO** - tasks needs to be done in order of having stable/finished app
**FIXME** - it is recommended to do the task in order of having better app/code
**TBD** - things needs to be discussed
**IMPORTANT** - really important notes

Leaving a reasonable comment without a flag is also accepted if making change in one peace of code may impact another, but isn't that important. Do not leave comment if your code is messy - clean it up!
For complex functions scenerio, prepare some unit tests as a form of documentation.
