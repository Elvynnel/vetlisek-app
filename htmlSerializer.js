// Docs: https://prismic.io/docs/technologies/html-serializer-reactjs

import { Elements } from 'prismic-reactjs';
import React, { createElement } from 'react';

// -- Function to add unique key to props
const propsWithUniqueKey = function (props, key) {
    return Object.assign(props || {}, { key });
};

// -- HTML Serializer
// This function will be used to change the way the HTML is loaded in RichText
export const htmlSerializer = function (type, element, content, children, key) {
    var props = {};
    switch (type) {
        case Elements.paragraph:
            return createElement(
                'p',
                propsWithUniqueKey({ className: 'text-paragraph' }, key),
                children,
            );
        case Elements.heading1:
            return createElement(
                'h1',
                propsWithUniqueKey({ className: 'text-heading1' }, key),
                children,
            );
        case Elements.heading2:
            return createElement(
                'h2',
                propsWithUniqueKey({ className: 'text-heading2' }, key),
                children,
            );
        case Elements.heading3:
            return createElement(
                'h3',
                propsWithUniqueKey({ className: 'text-heading3' }, key),
                children,
            );
        case Elements.heading4:
            return createElement(
                'h4',
                propsWithUniqueKey({ className: 'text-heading4' }, key),
                children,
            );
        case Elements.heading5:
            return createElement(
                'h5',
                propsWithUniqueKey({ className: 'text-heading5' }, key),
                children,
            );
        case Elements.heading6:
            return createElement(
                'h6',
                propsWithUniqueKey({ className: 'text-heading6' }, key),
                children,
            );
        // Don't wrap images in a <p> tag
        case Elements.image:
            props = { src: element.url, alt: element.alt || '' };
            return React.createElement('img', propsWithUniqueKey(props, key));

        // Add a class to hyperlinks
        // case Elements.hyperlink:
        //     const targetAttr = element.data.target
        //         ? { target: element.data.target }
        //         : {};
        //     const relAttr = element.data.target ? { rel: 'noopener' } : {};
        //     props = Object.assign(
        //         {
        //             className: 'link-class',
        //             href: element.data.url || linkResolver(element.data),
        //         },
        //         targetAttr,
        //         relAttr,
        //     );
        //     return React.createElement(
        //         'a',
        //         propsWithUniqueKey(props, key),
        //         children,
        //     );

        // Return null to stick with the default behavior
        default:
            return null;
    }
};
