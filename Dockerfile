FROM node:12

WORKDIR /app

ADD package.json .

COPY . /app

RUN npm install

RUN npm run build

CMD ["npm", "run", "start"]
