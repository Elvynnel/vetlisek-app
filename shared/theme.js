// Use https://chir.ag/projects/name-that-color if you need help with naming ;)
const paletteLight = {
    white: '#fff',
    whiteLike: '#F5F5F5',
    grayLight: '#d5d5d5',
    gray: '#848484',
    grayDark: '#6D6D6D',
    blackLike: '#242424',
    greenPersian: '#00A89C',
    greenPersianDark: '#004d47',
    orange: '#E86D47',
    darkBlue: '#0500a3',
};

// FIXME: Work on theme elements naming
export default {
    colors: {
        primary: paletteLight.greenPersian,
        secondary: paletteLight.orange,
        tertiary: paletteLight.whiteLike,

        textPrimary: paletteLight.blackLike,
        textSecondary: paletteLight.gray,
        textTertiary: paletteLight.grayDark,
        textQuarternary: paletteLight.whiteLike,

        backgroundPrimary: paletteLight.white,
        backgroundSecondary: paletteLight.blackLike,

        borderLight: paletteLight.grayLight,
        white: paletteLight.white,

        progress: paletteLight.greenPersianDark, // FIXME: Adjust progress color
    },
    typographies: {
        default: 'Open Sans',
        fancy: 'Georgia',
        prominent: 'Bebas Neue',
    },
    spacing: {
        xs: 0.384,
        s: 0.64,
        m: 1,
        l: 1.563,
        xl: 2.441,
        xxl: 3.815,
        view: {
            xs: 50,
            s: 117,
            m: 140,
            xxl: 494,
        },
    },
    // Major Third spec (1.25 scaling) https://type-scale.com/
    fontSize: {
        xxxxs: 0.384,
        xxxs: 0.512,
        xxs: 0.64,
        xs: 0.8,
        s: 1,
        m: 1.25,
        l: 1.563,
        xl: 1.953,
        xxl: 2.441,
        xxxl: 3.052,
    },
    // Perfect Fourth spec (1.33 scaling) https://type-scale.com/
    lineHeight: {
        xxs: 0.563,
        xs: 0.75,
        s: 1,
        m: 1.333,
        l: 1.777,
        xl: 2.369,
        xxl: 3.157,
        xxxl: 4.209,
    },
    fontWeight: {
        thin: 300,
        normal: 400,
        bold: 700,
    },
    deviceSize: {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200,
        xxl: 1600,
    },
    borderRadius: {
        xs: 5,
        s: 8,
        m: 15,
        l: 20,
        xl: 40,
    },
    transition: {
        quick: 0.15,
        default: 0.3,
    },
    zIndex: {
        negative: -1,
        lowest: 1,
        overlay: 2,
        sidebar: 3,
        header: 4,
        menuButton: 5,
        progressBar: 10,
    },
};
