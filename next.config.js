module.exports = {
    webpack: (config) => {
        config.module.rules.push({
            test: /\.(png|jpe?g|gif|svg)$/i,
            loader: 'file-loader',
            options: {
                outputPath: '../public/assets/',
                publicPath: 'assets/',
            },
        });
        return config;
    },
};
